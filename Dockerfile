FROM alpine:3.11

ENV JAVA_HOME /usr/lib/jvm/java-1.8-openjdk
ENV PATH $PATH:$JAVA_HOME/bin

RUN apk --no-cache add --virtual build-deps gcc g++ make ruby-dev zlib-dev && \
    apk --no-cache add asciidoctor openjdk8-jre ruby ruby-nokogiri ruby-json ttf-dejavu zlib && \
    gem install --no-document asciidoctor-diagram && \
    gem install --no-document asciidoctor-pdf --pre && \
    gem install --no-document asciidoctor-rouge && \
    gem install --no-document asciidoctor-revealjs && \
    gem install --no-document coderay && \
    gem install --no-document 'prawn', github: 'prawnpdf/prawn' && \
    gem install --no-document 'prawn-table', github: 'prawnpdf/prawn-table' && \
    gem cleanup && \
    apk del build-deps && \
    rm -rf /tmp/* /var/cache/apk/*

WORKDIR /data

VOLUME ["/data"]

RUN addgroup -g 1000 -S asciidoc && \
    adduser -u 1000 -S asciidoc -G asciidoc

COPY entrypoint.sh /

RUN chmod +x /entrypoint.sh

USER asciidoc

CMD ["sh", "/entrypoint.sh"]
