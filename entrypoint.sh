#!/bin/sh
set -e

asciidoctor -r asciidoctor-rouge -r asciidoctor-diagram *.adoc && \
asciidoctor-pdf -r asciidoctor-rouge -r asciidoctor-diagram *.adoc

exec "$@"
